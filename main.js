console.log('Only Master | Kelompok 4');

function volumePrismaSegitiga(a = 0, t = 0, tP = 0) {
  // volume = (1/2 x alas x tinggi) x tinggi prisma
  return 0.5 * a * t * tP;
}

function volumeBola(radius) {
  return (4 / 3) * radius ** 3;
}

const volKerucut = (jari, tinggi) => {
  return 3.14 * jari * jari * tinggi
}

function volumeTabung(phi = 3.14, r = 4, t = 12) {
  // r adalah jari-jari, t adalah tinggi tabung, 
  // rumus tabung = phi x jari-jari dikuadratkan x tinggi tabung
  return phi * (r**2) * t;
}

function volumeLimasSegiEmpat(a = 0, t = 0, tL = 0) {
  // volume = (1/3 x luas alas x tinggi limas)
  return (1 / 3) * a * t * tL;
}
